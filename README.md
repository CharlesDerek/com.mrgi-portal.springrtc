# Gradle Docker MRGI-Portal example

## All as one step:
```bash
make mrgi-run
```
*** ENV values need updating (ports) ***

## Other notes:
This is an example Java application that uses Spring Boot, Gradle and Docker WebRTC build with internal Stun and Turn.

add repos as submodules!!!
```
git submodule add git@gitlab.com:CharlesDerek/com.mrgi-portal.springrtc.client.git
git submodule add git@gitlab.com:CharlesDerek/com.mrgi-portal.springrtc.rest.git
git submodule add git@gitlab.com:CharlesDerek/com.mrgi-portal.springrtc.rtc_node.git
git submodule add git@gitlab.com:CharlesDerek/com.mrgi-portal.springrtc.rtc_go.git
```

- go into each repo and run the following:
```
git branch -r | grep -v '\->' | while read remote; do git branch --track "${remote#origin/}" "$remote"; done
git fetch --all
git pull --all
```
(make sure the parent branch is ready for deployment)



- **[com.mrgi-portal.springrtc (Current)](https://gitlab.com/CharlesDerek/mrgi-portal/springrtc)**
  - [client](https://gitlab.com/CharlesDerek/com.mrgi-portal.springrtc.client/)
  - [rest](https://gitlab.com/CharlesDerek/mrgi-portal/springrtc.rest)
  - [rtc_node](https://gitlab.com/CharlesDerek/mrgi-portal/springrtc.rtc_node)
  - [rtc_go](https://gitlab.com/CharlesDerek/mrgi-portal/springrtc.rtc_go)

---
### Ignore the following section if you are not interested in multi-stage builds:

## Create a multi-stage docker image
To compile and package using Docker multi-stage builds

```bash
docker build . -t this-is-a-test
```

## Create a Docker image packaging an existing jar

```bash
./gradlew build
docker build . -t this-is-a-pre-packaged-test -f Dockerfile.only-package
```

## To run the docker image

```bash
docker run -p 8080:8080 this-is-a-test
```

And then visit http://localhost:8080 in your browser.

## To use this project at Athernex and MRGI Project - Gradle Scaffold

More details can be found in [Athernex documentation](https://athernex.com/documentation/index/)

Notes {
    State: 200
}
